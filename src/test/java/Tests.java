import Engine.ChessBoard.ChessBoard;
import Engine.Moves_and_Game.Chess;
import Engine.Moves_and_Game.Moves;
import Engine.Pieces.Figure;
import Engine.Pieces.FigureMoving;
import Engine.Pieces.FigureOnPiece;
import Engine.Pieces.Piece;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;

public class Tests {

    /**
     * Test the right of putting check you after your move, depends on FEN of the board,
     * change FEN as you like for tests, now there will be a check from black Queen e7
     * after white Queen goes f2
     * */
    @Test
    public void testIsCheckAfterMove() {
        ChessBoard board = new ChessBoard("rnb1kbnr/1111q111/8/8/8/8/1111Q111/RNB1KBNR w KQkq - 0 1");
        assertTrue(board.IsCheckAfterMove(new FigureMoving(new FigureOnPiece(Figure.whiteQueen, new Piece("e2")), new Piece("f2"), Figure.none)));
    }

    /**
     * Tests the right of move En Passant and the right deleting of pawn from board
     * */
    @Test
    public void testDeletingBadPawnAfterEnPassant(){
        ChessBoard board = new ChessBoard("rnbqkbnr/ppp1pppp/8/111pP111/8/8/PPPP1PPP/RNBQKBNR w KQkq - 0 1");

        board = board.Move(new FigureMoving(new FigureOnPiece(Figure.whitePawn, new Piece("e5")), new Piece("d6"), Figure.none));
        board = board.DeletingBadPawnAfterEnPassant(new FigureMoving(new FigureOnPiece(Figure.whitePawn, new Piece("e5")),new Piece("d6"), Figure.none), board.fen);
        ChessBoard controlBoard = new ChessBoard("rnbqkbnr/ppp1pppp/111P1111/11111111/11111111/11111111/PPPP1PPP/RNBQKBNR b - - 0 1");

        assertEquals(board.fen, controlBoard.fen);
        assertArrayEquals(board.figures, controlBoard.figures);
        assertEquals(board.moveColor, controlBoard.moveColor);
        assertEquals(board.moveNumber, controlBoard.moveNumber);
    }

    /**
     * Tests the right of saving elements after move and if it makes move right
     * but here Rook is not moved, he stays his position and then in Move Rook moves
     * */
    @Test
    public void testChessboardMoveWhileCastling(){
        ChessBoard board = new ChessBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQK11R w KQkq - 0 1");
        ChessBoard controlBoard = new ChessBoard("rnbqkbnr/pppppppp/11111111/11111111/11111111/11111111" +
                "/PPPPPPPP/RNBQ11KR b - - 0 1");
        board = board.MoveWhileCastling(new FigureMoving(new FigureOnPiece(Figure.whiteKing, new Piece("e1")),new Piece("g1"),Figure.none), board.fen);

        assertEquals(board.moveColor, controlBoard.moveColor);
        assertEquals(board.moveNumber, controlBoard.moveNumber);
        assertEquals(board.fen, controlBoard.fen);
        assertArrayEquals(board.figures, controlBoard.figures);
    }

    /**
     * Tests right of implementation of getting figure from board,
     * board for x and y looks like:
     *    0  1  2  3  4  5  6  7 - x
     * 7  r  n  b  q  k  b  n  r  8
     * 6  p  p  p  p  p  p  p  p  7
     * 5  .  .  .  .  .  .  .  .  6
     * 4  .  .  .  .  .  .  .  .  5
     * 3  .  .  .  .  .  .  .  .  4
     * 2  .  .  .  .  .  .  .  .  3
     * 1  P  P  P  P  P  P  P  P  2
     * 0  R  N  B  Q  K  B  N  R  1
     * y  a  b  c  d  e  f  g  h
     * */
    @Test
    public void testGetFigure(){
        Chess chess = new Chess("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        assertEquals(chess.GetFigureAt(4, 1), Figure.whitePawn.GetText());
    }

    /**
     * Test for controling how much moves it returns for the first move
     * If the size of array in the first move equals 20
     * (20 moves should be in the begining of the game)
     * */
    @Test
    public void testAllMoves() {
        // You can change your chess board by fen and then change
        // numOfMoves to control how much moves it will give
        Chess chess = new Chess("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        int numOfMoves = 20;
        chess.FindAllMoves();

        assertEquals(chess.allMoves.size(), numOfMoves);
    }
    /**
     * Tests the right of algorith finding a bad King for the first move(white move)
     * bad King is black King and hi is in piece x = 4 y = 7, down there is a chessboard with
     * shown axes
     * */
    @Test
    public void testFindBadKing(){
        ChessBoard board = new ChessBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        Piece pieceOfBadKing= board.FindBadKing();
        Piece control = new Piece("e8");

        assertEquals(control.x, pieceOfBadKing.x);
        assertEquals(control.y, pieceOfBadKing.y);
    }

    /**
     * Test for checking of right implementation of changing
     * string to coordinates functiom
     * */
    @Test
    public void testPieceConstructor() {
        Piece e2 = new Piece("e2");

        assertEquals(e2.x, 4);
        assertEquals(e2.y, 1);
    }

    /**
     * Tests the right of implementation of method CanMove
     * if the Move is possible
     * */
    @Test
    public void testCanMove() {
        ChessBoard board = new ChessBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        Moves moves = new Moves(board);
        FigureOnPiece fp = new FigureOnPiece(Figure.whitePawn, new Piece("e2"));
        FigureMoving fm = new FigureMoving(fp, new Piece("e4"), Figure.none);

        assertTrue(moves.CanMove(fm));
    }

    /**
     * Tests the right of saving elements after move and if it makes move right
     * */
    @Test
    public void testChessboardMove() {
        ChessBoard board = new ChessBoard("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
        board = board.Move(new FigureMoving(new FigureOnPiece(Figure.whitePawn, new Piece("e2")), new Piece("e4"), Figure.none));

        ChessBoard controlBoard = new ChessBoard("rnbqkbnr/pppppppp/11111111/11111111/1111P111/11111111/PPPP1PPP/RNBQKBNR b - - 0 1");

        assertSame(board.moveColor, controlBoard.moveColor);
        assertEquals(board.moveNumber, controlBoard.moveNumber);
        assertEquals(board.fen, controlBoard.fen);
        assertArrayEquals(board.figures, controlBoard.figures);
    }

    /**
     * Tests the right of saving elements after move and if it makes move right
     * Tested on EN PASSANT
     * */
    @Test
    public void testChessMove() {
        Chess chess = new Chess("rnbqkbnr/ppp1pppp/8/111pP111/8/8/PPPP1PPP/RNBQKBNR w KQkq - 0 1");
        Chess controlChess = new Chess("rnbqkbnr/ppp1pppp/111P1111/11111111/11111111/11111111/PPPP1PPP/RNBQKBNR b - - 0 1");
        chess = chess.Move("Pe5d6");

        assertEquals(chess.board.fen, controlChess.board.fen);
        assertEquals(chess.board.moveNumber, controlChess.board.moveNumber);
        assertSame(chess.board.moveColor, controlChess.board.moveColor);
        assertArrayEquals(chess.board.figures, controlChess.board.figures);
        assertArrayEquals(new ArrayList[]{chess.allMoves}, new ArrayList[]{controlChess.allMoves});
        assertEquals(chess.fen, controlChess.fen);
    }

    /**
     * Tests the right of putting a check, depends on FEN of the board,
     * change FEN as you like for tests, now there is a check from black Queen e7
     * */
    @Test
    public void testIsCheck() {
        ChessBoard board = new ChessBoard("rnb1kbnr/1111q111/8/8/8/8/8/RNBQKBNR w KQkq - 0 1");

        assertTrue(board.IsCheck());
    }
    /**
     *
     * */
    @Test
    public void testFigureMovingConstructor() {
        FigureMoving fm = new FigureMoving("Pe7e8Q");

        assertSame(fm.figure, Figure.whitePawn);
        assertEquals(4, fm.from.x);
        assertEquals(6, fm.from.y);
        assertEquals(4, fm.to.x);
        assertEquals(7, fm.to.y);
        assertSame(fm.promotion, Figure.whiteQueen);
    }
}
