package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class You_lose extends JFrame{
    private final static Dimension PIECE_PANEL_DIMENSION = new Dimension(100,100);
    private static final long serialVersionUID = 1L;

    /**
     * Special Frame for messages of lose, appears when somone loses, wins or time is over
     * @param CheckMate_or_StaleMate String to push it to message when it will be called
     * */
    public You_lose(String CheckMate_or_StaleMate) throws IOException {
        super(CheckMate_or_StaleMate);
        final BufferedImage image = ImageIO.read(new File(Table.defaultPieceImagePath + "CheckMate" + ".png"));
        Image dimage = image.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        JButton button = new JButton(new ImageIcon(dimage));
        button.setPreferredSize(PIECE_PANEL_DIMENSION);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        // Создание панели содержимого с размещением кнопок
        JPanel contents = new JPanel();

        contents.add(button);
        setContentPane(contents);
        // Определение размера и открытие окна
        setSize(350, 150);
        setVisible(true);
    }
}
