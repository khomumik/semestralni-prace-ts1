package GUI;

import javax.swing.*;

import java.awt.*;

public class NotUsed_TextFieldTest extends JFrame
{
    public JTextField bigField1, bigField2, bigField3, bigField4, bigField5, bigField6, bigField7;

    public NotUsed_TextFieldTest()
    {
        super("Portable Game Notation");
        // Создание текстовых полей
        bigField1 = new JTextField(25);
        bigField1.setToolTipText("Event");
        bigField1.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField1.setHorizontalAlignment(JTextField.LEFT);

        bigField2 = new JTextField( 25);
        bigField2.setToolTipText("Site");
        bigField2.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField2.setHorizontalAlignment(JTextField.LEFT);

        bigField3 = new JTextField( 25);
        bigField3.setToolTipText("Date");
        bigField3.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField3.setHorizontalAlignment(JTextField.LEFT);

        bigField4 = new JTextField( 25);
        bigField4.setToolTipText("Round");
        bigField4.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField4.setHorizontalAlignment(JTextField.LEFT);

        bigField5 = new JTextField( 25);
        bigField5.setToolTipText("White");
        bigField5.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField5.setHorizontalAlignment(JTextField.LEFT);

        bigField6 = new JTextField( 25);
        bigField6.setToolTipText("Black");
        bigField6.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField6.setHorizontalAlignment(JTextField.LEFT);

        bigField7 = new JTextField( 25);
        bigField7.setToolTipText("Result");
        bigField7.setFont(new Font("Dialog", Font.PLAIN, 14));
        bigField7.setHorizontalAlignment(JTextField.LEFT);

        JLabel event = new JLabel("Event:");
        JLabel site = new JLabel("Site:");
        JLabel date = new JLabel("Date:");
        JLabel round = new JLabel("Round:");
        JLabel white = new JLabel("White:");
        JLabel black = new JLabel("Black:");
        JLabel result = new JLabel("Result:");


        //JPanel contents = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel contents = new JPanel(new GridLayout(7,2));
        contents.add(event);
        contents.add(bigField1);

        contents.add(site);
        contents.add(bigField2, BorderLayout.WEST);

        contents.add(date);
        contents.add(bigField3, BorderLayout.WEST);

        contents.add(round);
        contents.add(bigField4, BorderLayout.WEST);

        contents.add(white);
        contents.add(bigField5, BorderLayout.WEST);

        contents.add(black);
        contents.add(bigField6, BorderLayout.WEST);

        contents.add(result);
        contents.add(bigField7, BorderLayout.WEST);

        setContentPane(contents);
        // Определяем размер окна и выводим его на экран
        setSize(350, 250);
        setVisible(true);
    }
}
