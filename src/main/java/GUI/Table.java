package GUI;

import Engine.ChessBoard.ChessBoard;

import Engine.Pieces.Figure;
import Engine.Pieces.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


import static Engine.Moves_and_Game.Game.chess;
import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

public class Table extends JFrame{
    private final JFrame gameFrame;
    private final BoardPanel boardPanel;
    private ChessBoard chessboard;
    private final TimerPanel timerPanel;

    private final File file = new File("C://Users//User//IdeaProjects//Chess", "ClassicPGN.pgn");
    private final String path = "C://Users//User//IdeaProjects//Chess//ClassicPGN.pgn";

    private Piece SourcePiece;
    private Piece destinationPiece;
    private FigureOnPiece humanMovedPiece;

    private int numOfMoves = 1;

    private final static Dimension OUTER_FRAME_DIMENSION = new Dimension(700, 600);
    private final static Dimension BOARD_PANEL_DIMENSION = new Dimension(400, 350);
    private final static Dimension PIECE_PANEL_DIMENSION = new Dimension(10,10);
    private final static Dimension TIMER_PANEL_DIMENSION = new Dimension(80,50);

    private static final Logger logger = Logger.getLogger(Table.class.getName());

    public static String defaultPieceImagePath = "src/main/resources/";
    public static ArrayList<String > listOfMoves = new ArrayList<String>();

    private final Color darkPieceColor = Color.decode("#4E7838");
    private final Color lightPieceColor = Color.decode("#fff5b0");

    /**
     * Constructor for GUI, has inside JmenuBar, TimerPanel
     * @param chessBoard current chessboard
     * */
    public Table(ChessBoard chessBoard) throws IOException, InterruptedException {
        this.gameFrame = new JFrame("JChess");
        this.gameFrame.setLayout(new BorderLayout());
        final JMenuBar tableMenuBar = createTableMenuBar();
        this.gameFrame.setJMenuBar(tableMenuBar);
        this.gameFrame.setSize(OUTER_FRAME_DIMENSION);
        this.gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.chessboard = chessBoard;
        this.boardPanel = new BoardPanel();
        this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);

        this.timerPanel = new TimerPanel();
        this.gameFrame.add(this.timerPanel, BorderLayout.EAST);
        this.gameFrame.setVisible(true);

    }

    /**
     * Creates JmenuBar
     * @return JmenuBar
     * */
    private JMenuBar createTableMenuBar() {
        final JMenuBar tableMenuBar = new JMenuBar();
        tableMenuBar.add(createFileMenu());
        return tableMenuBar;
    }

    /**
     * Creates all FileMenu File, includes: Save game as PGN File,
     * Load game from PGN File and Exit
     * */
    private JMenu createFileMenu() {
        final JMenu fileMenu = new JMenu("File");
        final JMenuItem savePGN = new JMenuItem("Save game as PGN File");
        savePGN.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try {
                    logger.info("Writing moves to PGN");
                    writeMovesToPGN();
                } catch (FileNotFoundException fileNotFoundException) {
                    logger.log(Level.SEVERE,"Eror: I didn't find file to save PGN", fileNotFoundException);
                }
            }
        });
        final JMenuItem loadPGN = new JMenuItem("Load game from PGN File");
        assert false;
        loadPGN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<String> listOfMovesFromPGN = null;
                for(int i = 0; i <= numOfMoves; i++) {
                    try {
                        logger.info("Loading moves from PGN");
                        listOfMovesFromPGN = loadMovesFromPGN();
                    } catch (IOException ioException) {
                        logger.log(Level.SEVERE, "Eror messege", ioException);
                    }
                    for (String move : listOfMovesFromPGN) {
                        chess = chess.Move(move);
                        listOfMoves.add(move);

                        chessboard = chess.board;
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                logger.info("Drawing a board panel");
                                boardPanel.drawBoard(chess.board);
                            } catch (IOException ioException) {
                                logger.log(Level.SEVERE, "Eror messege", ioException);
                            }
                        }
                    });
                }
            }
        });

        final JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logger.info("Exit...");
                System.exit(0);
            }
        });
        fileMenu.add(loadPGN);
        fileMenu.add(savePGN);
        fileMenu.add(exitMenuItem);
        return fileMenu;
    }

    /**
     * For PGN file to skip first parts of it with [...]
     * @param gameText String
     * @return boolean, true if String begins and ends with "[" "]"
     * */
    private static boolean isTag(final String gameText) {
        return gameText.startsWith("[") && gameText.endsWith("]");
    }

    /**
     * Function to make button realize loading game from PGN file
     * (Note: it's possible to do only in the begining)
     * @return Arraylist of Strings (moves)
     * */
    private ArrayList<String> loadMovesFromPGN() throws IOException {
        ArrayList<String> listOfMovesFromPGN= new ArrayList<String>();

        FileReader reader = new FileReader(file);
        final BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while((line = br.readLine()) != null){
            if(!line.isEmpty()){
                if(!isTag(line)){
                    final String[] ending = line.split(" ");
                    final String outcome = ending[ending.length - 1];
                    line = line.replaceAll(outcome, "");
//                    for(int i = 1; i<= chessboard.moveNumber; i++){
//                        String numWithDot = i + ".";
//
//                        line = line.replaceAll(numWithDot, "");
//                    }
                    final String gameText = line.trim();
                    if(!gameText.isEmpty()) {
                        chess.FindAllMoves();
                        String[] list = line.split(" ");
                        numOfMoves = list.length;
                        for (String moveFromPGN : list) {
                            moveFromPGN = moveFromPGN.replaceAll(chessboard.moveNumber + ".", "");
                            if(moveFromPGN.length() == 2){
                                moveFromPGN = chessboard.moveColor == Engine.Pieces.Color.white? "P" + moveFromPGN : "p" + moveFromPGN;

                            } else if(moveFromPGN.equals("O-O")){
                                moveFromPGN = chessboard.moveColor == Engine.Pieces.Color.white? "Kg1" : "kg8";
                            } else if(moveFromPGN.equals("O-O-O")){
                                moveFromPGN = chessboard.moveColor == Engine.Pieces.Color.white? "Kc1" : "kc8";
                            }
                            for (FigureMoving fm : chess.allMoves) {
                                String piece2String_theEndOfMove = fm.figure.GetText() + chess.WhatChar(fm.to.x) + (fm.to.y+1);
                                if (moveFromPGN.equals(piece2String_theEndOfMove)) {
                                    String no = fm.figure.GetText() + chess.WhatChar(fm.from.x) + (fm.from.y+1) +
                                    moveFromPGN.substring(1,3);
                                    listOfMovesFromPGN.add(no);
                                }
                            }
                        }
                    }
                }
            }
        }
        return listOfMovesFromPGN;
    }

    /**
     * Function for saving only moves to PGN file
     * */
    private void writeMovesToPGN() throws FileNotFoundException {
        String whiteMove;
        String blackMove;
        PrintWriter delete = new PrintWriter(file);
        delete.print("");
        delete.close();
        try (FileWriter writer = new FileWriter(file, true)) {
            logger.fine("Writing tags to PGN");
            writer.write("[Event \"Cup of Hogwarts \"]\n");
            writer.write("[Site \"Hogwarts ENG\"]\n");
            writer.write("[Date \"2012.12.12\"]\n");
            writer.write("[Round \"1\"]\n");
            writer.write("[White \"Potter, Harry\"]\n");
            writer.write("[Black \"Granger, Hermione\"]\n");
            writer.write("[Result \"0-0\"]\n");
            writer.write("\n");

            writer.flush();
        } catch (IOException ex) {
            logger.info(ex.getMessage());
        }
        for(int i = 1; i <= chessboard.moveNumber; i++){
            if(i == chessboard.moveNumber && listOfMoves.size() % 2 == 1){
                whiteMove = listOfMoves.get(2*i-2);

                try (FileWriter writer = new FileWriter(file, true)) {
                    logger.fine("Writing Castling from PGN");
                    if(whiteMove.equals("O-O-O") || whiteMove.equals("O-O")){
                        writer.write(i + "." +
                                whiteMove + " ");
                    }else{
                        writer.write(i + "." + (whiteMove.startsWith("P") ? "" : whiteMove.substring(0,1)) +
                                whiteMove.substring(3,5) + " ");
                    }


                    writer.flush();
                } catch (IOException ex) {
                    logger.info(ex.getMessage());
                }
            }else if (i != chessboard.moveNumber){
                whiteMove = listOfMoves.get(2*i-2);
                blackMove = listOfMoves.get(2*i-1);

                try (FileWriter writer = new FileWriter(file, true)) {
                    logger.fine("Writing moves to PGN");
                    if((whiteMove.equals("O-O-O") || whiteMove.equals("O-O")) && !(blackMove.equals("O-O-O") || blackMove.equals("O-O"))){
                        writer.write(i + "." + whiteMove +
                                (blackMove.startsWith("p") ? "" : blackMove.substring(0, 1)) +
                                blackMove.substring(3, 5) + " ");
                    }else if((blackMove.equals("O-O-O") || blackMove.equals("O-O")) && !(whiteMove.equals("O-O-O") || whiteMove.equals("O-O"))){
                        writer.write(i + "." + (whiteMove.startsWith("P") ? "" : whiteMove.substring(0, 1)) +
                                whiteMove.substring(3, 5) + " " +
                                blackMove + " ");
                    } else if((whiteMove.equals("O-O-O") || whiteMove.equals("O-O")) && (blackMove.equals("O-O-O") || blackMove.equals("O-O"))){
                        writer.write(i + "." + whiteMove + " " + blackMove + " ");
                    }else{
                        writer.write(i + "." + (whiteMove.startsWith("P") ? "" : whiteMove.substring(0, 1)) +
                                whiteMove.substring(3, 5) + " " +
                                (blackMove.startsWith("p") ? "" : blackMove.substring(0, 1)) +
                                blackMove.substring(3, 5) + " ");
                    }
                    writer.flush();
                } catch (IOException ex) {
                    logger.info(ex.getMessage());
                }
            }
        }
        try (FileWriter writer = new FileWriter(file, true)) {
            logger.fine("Writing the end of PGN file");
            writer.write("0-0");

            writer.flush();
        } catch (IOException ex) {
            logger.info(ex.getMessage());

        }
    }

    /**
     * Class for Timers in GUI
     * */
    private class TimerPanel extends JPanel{
        JLabel TimerWhiteLabel = new JLabel();
        JLabel TimerBlackLabel = new JLabel();
        int count = 900;// 1800 - 30 min
        /**
         * Makes Timer
         * */
        TimerPanel(){
            super(new GridLayout(8,1));
            add(TimerBlackLabel);
            add(TimerWhiteLabel);
            final Object sync = new Object();

            Thread TimerWhite = new Thread(new Runnable() {

                @Override
                public void run() {
                    long startTime = System.currentTimeMillis();
                    long elapsedSeconds = 0;
                    long elapsedTime = 0;
                    while (elapsedSeconds != count) {
                        synchronized (sync){
                            if(chessboard.moveColor != Engine.Pieces.Color.white){
                                logger.fine("TimerWhite: notifyALL");
                                sync.notifyAll();
                                try {
                                    logger.fine("TimerWhite is waiting");
                                    sync.wait();

                                } catch (InterruptedException e) {
                                    logger.log(Level.SEVERE, "Eror messege", e);
                                }
                            }
                        }
                        startTime = System.currentTimeMillis();
                        try {
                            logger.fine("Sleep for a second for timer");
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            logger.log(Level.SEVERE, "Eror messege", e);
                        }

                        elapsedTime += System.currentTimeMillis() - startTime;

                        elapsedSeconds = elapsedTime / 1000;
                        long secondsDisplay = elapsedSeconds % 60;
                        long elapsedMinutes = elapsedSeconds / 60;

                        TimerWhiteLabel.setText("WHITE: " + elapsedMinutes + "." + secondsDisplay);
                    }
                    try {
                        logger.info("Sorry, time is over...");
                        new You_lose("Time is over, " + (chessboard.moveColor == Engine.Pieces.Color.black ?
                                Engine.Pieces.Color.white : Engine.Pieces.Color.black) + " won!!!");
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, "Eror messege", e);
                    }
                }
            });
            Thread TimerBlack = new Thread(new Runnable() {

                @Override
                public void run() {

                    long startTime = System.currentTimeMillis();
                    long elapsedSeconds = 0;
                    long elapsedTime = 0;

                    while (elapsedSeconds != count) {
                        synchronized (sync){
                            if(chessboard.moveColor != Engine.Pieces.Color.black){
                                logger.fine("TimerBlack: notifyALL");
                                sync.notifyAll();
                                try {
                                    logger.fine("TimerBlack is waiting");
                                    sync.wait();
                                } catch (InterruptedException e) {
                                    logger.log(Level.SEVERE, "Eror messege", e);
                                }
                            }
                        }
                        startTime = System.currentTimeMillis();
                        try {
                            logger.fine("Sleep for a second for timer");
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            logger.log(Level.SEVERE, "Eror messege", e);
                        }
                        elapsedTime += System.currentTimeMillis() - startTime;

                        elapsedSeconds = elapsedTime / 1000;
                        long secondsDisplay = elapsedSeconds % 60;
                        long elapsedMinutes = elapsedSeconds / 60;

                        TimerBlackLabel.setText("BLACK: " + elapsedMinutes + "." + secondsDisplay);
                    }
                    try {
                        logger.info("Sorry, time is over...");
                        new You_lose("Time is over, " + (chessboard.moveColor == Engine.Pieces.Color.black ?
                                Engine.Pieces.Color.white : Engine.Pieces.Color.black) + " won!!!");
                    } catch (IOException e) {
                        logger.log(Level.SEVERE, "Eror messege", e);
                    }
                }
            });
            TimerWhite.start();
            TimerBlack.start();
            setPreferredSize(TIMER_PANEL_DIMENSION);
            validate();
        }
    }
    /**
     * Class for Chessboard in GUI
     * */
    private class BoardPanel extends JPanel{
        final ArrayList<PiecePanel> boardPieces;
        /**
         * Constructor
         * Makes board by GridLayout(8,8)
         * */
        BoardPanel() throws IOException {
            super(new GridLayout(8,8));
            this.boardPieces = new ArrayList<>();
            for(int column = 7; column >= 0;column--){
                for(int row = 0; row< 8; row++){
                    final PiecePanel piecePanel= new PiecePanel(this, column, row);
                    this.boardPieces.add(piecePanel);
                    add(piecePanel);
                }

            }
            setPreferredSize(BOARD_PANEL_DIMENSION);
            validate();
        }

        /**
         * Function of drawing board in JFrame
         * @param chessboard current chessboard
         * */
        public void drawBoard(final ChessBoard chessboard) throws IOException {
            removeAll();
            for(final PiecePanel piecePanel: boardPieces){
                piecePanel.drawPiece(chessboard);
                add(piecePanel);
                validate();
                repaint();
            }
        }
    }
    /**
     * Class for Pieces in BoardPanel
     * */
    private class PiecePanel extends  JPanel{
        private final int column;
        private final int row;
        private final Piece piece;

        /**
         * Constructor for
         * Makes pieces in BoardPanel inheriting GridBagLayout
         * @param boardPanel BoardPanel
         * @param column int
         * @param row int
         * */
        PiecePanel(final  BoardPanel boardPanel, final int column, int row) throws IOException {
            super(new GridBagLayout());
            this.row = row;
            this.column = column;
            this.piece = new Piece(row, column);

            setPreferredSize(PIECE_PANEL_DIMENSION);
            assignPieceColor();
            assignPieceIcon(chessboard);

            Thread Mouse = new Thread(new Runnable() {
                @Override
                public void run() {
                    addMouseListener(new MouseListener() {
                        @Override
                        public void mouseClicked(final MouseEvent e) {
//                            if(chessboard.moveColor == Engine.Pieces.Color.white){
//                                timer_white.start();
//                            } else {
//
//                                timer_black.start();
//                            }
                            if(isRightMouseButton(e)){
                                logger.info("Right Mouse Clicked");
                                SourcePiece = null;
                                destinationPiece = null;
                                humanMovedPiece = null;
                            }else if(isLeftMouseButton(e)){
                                logger.info("Left Mouse Clicked");
                                if(SourcePiece == null){
                                    FigureOnPiece fp = new FigureOnPiece(chessboard.GetFigureAt(piece),piece);
                                    SourcePiece = fp.piece;
                                    humanMovedPiece = new FigureOnPiece(chessboard.GetFigureAt(SourcePiece), SourcePiece);
                                    if (humanMovedPiece.figure == Figure.none){
                                        SourcePiece = null;
                                    }
                                }else{
                                    FigureOnPiece fp = new FigureOnPiece(chessboard.GetFigureAt(piece), piece);
                                    destinationPiece = fp.piece;
                                    if(destinationPiece != SourcePiece){
                                        String move = chessboard.GetFigureAt(SourcePiece).GetText() + chess.WhatChar(SourcePiece.x) +
                                                (SourcePiece.y + 1) + chess.WhatChar(destinationPiece.x) + (destinationPiece.y + 1);

                                        chess = chess.Move(move);
                                        if(move.startsWith("K") || move.startsWith("k")){
                                            final int DeltaX = (move.charAt(1) - 'a') - (move.charAt(3) - 'a');
                                            if(Math.abs(DeltaX) == 2){
                                                 if(DeltaX == -2) {
                                                     listOfMoves.add("O-O");
                                                 } else {
                                                     listOfMoves.add("O-O-O");
                                                 }
                                             }
                                        }else {
                                            logger.finer("Move is added to listOfMoves");
                                            listOfMoves.add(move);
                                        }
                                        chessboard = chess.board;

                                        // After checkmate or stale mate returns the window named what happened and who won
                                        ArrayList<String> list = chess.GetAllMoves();
                                        if(chess.IsCheck() && list.size() == 0){
                                            try {
                                                logger.info("ChecMate, you lose!");
                                                new You_lose("CheckMate, " + (chessboard.moveColor == Engine.Pieces.Color.black ?
                                                        Engine.Pieces.Color.white : Engine.Pieces.Color.white) + " won!!!");
                                            } catch (IOException ioException) {
                                                logger.log(Level.SEVERE, "Eror messege", ioException);
                                            }
                                        } else if(list.size()== 0 && !chess.IsCheck()){
                                            try {
                                                logger.info("ChecMate, you lose!");
                                                new You_lose("StaleMate, " + (chessboard.moveColor == Engine.Pieces.Color.black ?
                                                        Engine.Pieces.Color.white : Engine.Pieces.Color.white) + " won!!!");
                                            } catch (IOException ioException) {
                                                logger.log(Level.SEVERE, "Eror messege", ioException);
                                            }
                                        }
                                    }
                                    SourcePiece = null;
                                    destinationPiece = null;
                                    humanMovedPiece = null;

                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                logger.fine("Drawing a board panel");
                                                boardPanel.drawBoard(chess.board);
                                            } catch (IOException ioException) {
                                                ioException.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void mousePressed(final MouseEvent e) {

                        }

                        @Override
                        public void mouseReleased(final MouseEvent e) {

                        }

                        @Override
                        public void mouseEntered(final MouseEvent e) {

                        }

                        @Override
                        public void mouseExited(final MouseEvent e) {

                        }
                    });
                }
            });
            Mouse.start();
            validate();
        }

        /**
         * Sets Icon image for chess figures(draw figures on chessboard)
         * @param board current chessboard
         * */
        private void assignPieceIcon(final ChessBoard board) throws IOException {
            this.removeAll();
            if(board.GetFigureAt(this.piece) != Figure.none){
                final BufferedImage image = ImageIO.read(new File(defaultPieceImagePath +
                        Figure.GetColor(board.GetFigureAt(this.piece)).toString().substring(0,1) +
                        board.GetFigureAt(this.piece).GetText() + ".png"));
                this.add(new JLabel(new ImageIcon(image)));
            }
        }

        /**
         * Sets Color for Pieces(draw chessboard)
         * */
        private void assignPieceColor() {
            if((this.row + this.column)%2 == 1){
                setBackground(lightPieceColor);
            } else {
                setBackground(darkPieceColor);
            }
        }

        /**
         * Draws all chessboard with pieces and images of figures
         * @param chessboard current chessboard
         * */
        public void drawPiece(final ChessBoard chessboard) throws IOException {
            assignPieceColor();
            assignPieceIcon(chessboard);
            validate();
            repaint();

        }
    }
}
