package Engine.ChessBoard;

import Engine.Moves_and_Game.Moves;
import Engine.Pieces.*;

import java.util.ArrayList;
import java.util.logging.Logger;

import static Engine.Pieces.Figure.*;

public class ChessBoard {
    public String fen;
    public Figure[][] figures;
    public Color moveColor;
    public int moveNumber;

    private static final Logger logger = Logger.getLogger(ChessBoard.class.getName());

    public ChessBoard(String fen) {
        this.fen = fen;
        this.figures = new Figure[8][8];
        Init();

    }

    /**
     * Initialization of chessboard with help of FEN
     * */
    void Init() {
        //"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
        // 0                                           1 2    3 4 5
        // 1-who first 2-Castling conditions 3-битое поле 5- номер хода
        logger.finer("Initialization of chessboard");
        String[] parts = fen.split(" ");
        if (parts.length != 6)return;
        InitFigures(parts[0]);  //inicializace figurek dle FENa
        moveColor = parts[1].equals("b") ? Color.black:Color.white; //Inicializace barvy dle FENa
        moveNumber = Integer.parseInt(parts[5]);
    }

    /**
     * It sets figures on their places thanks to FEN
     * @param data has inside the first part of FEN
     * */
    void InitFigures(String data){
        for(int j = 8; j >= 2; j--){
            data = data.replace(Integer.toString(j), Integer.toString(j-1) + "1");
        }
        String[] lines = data.split("/");
        for(int y = 7; y >= 0; y--){
            for(int x = 0; x < 8; x++){
                figures[x][y] = Figure.GetFigure(lines[7-y].charAt(x));
            }
        }
    }

    /**
     * GenerateFEN helps you to have progress in chess,
     * it generates FEN  and sets it to variable initialized here:-)
     * */
    public void GenerateFEN(){
        fen = FenFigures() + " " +
                (moveColor == Color.white ? "w" : "b") + " - - 0 " +
                moveNumber;
    }

    /**
     * Method help GenerateFEN to do his work,
     * exactly here the main part of FEN is made
     * ("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR)
     * */
    String FenFigures(){
        StringBuilder sb = new StringBuilder();
        for(int y = 7; y >= 0; y--){
            for(int x = 0; x < 8; x++){
                sb.append(figures[x][y] == Figure.none ? '1' : figures[x][y].GetText());
            }
            if(y>0){ sb.append("/"); }
        }

        return sb.toString();
    }

    /**
     * @param piece - coordinates x and y in one constructor
     * @return figure what is on given piece
     * */
    public Figure GetFigureAt(Piece piece){
        logger.finer("Gives a figure");
        if(piece.OnBoard()){
            return figures[piece.x][piece.y];
        }
        return Figure.none;
    }

    /**
     * Function sets given figure on given piece
     * @param piece - coordinates x and y in one constructor
     * @param figure kind of figure described in class Figure
     * */
    public void SetFigureAt(Piece piece, Figure figure){
        logger.finer("Sets a figure");
        if(piece.OnBoard()){
            figures[piece.x][piece.y] = figure;
        }
    }

    /**
     * Function sets none figure on previous piece and actual on next piece(where it goes)
     * Also here FEN changing after all done moves
     * @param fm - (constructor from class FigureMoving) moving figure
     *          with coordinates from, where, what kind of figure and promotion
     * */
    public ChessBoard Move(FigureMoving fm) {
        logger.fine("New move or checking check");
        ChessBoard next = new ChessBoard(fen);
        next.SetFigureAt(fm.from, Figure.none);
        next.SetFigureAt(fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);
        if (moveColor == Color.black) {
            next.moveNumber++;
        }
        next.moveColor = moveColor.FlipColor();
        next.GenerateFEN(); //we change FEN to remember steps
        return next;
    }

    /**
     * The same function as previous Move except giving new FEN to avoid problem with
     * incorrect setting of Rook, King and move Color
     * @param fm - (constructor from class FigureMoving) moving figure
     *          with coordinates from, where, what kind of figure and promotion
     * @param CastlingFen - FEN while Castling because other way we cann't do second move by Rook
     * */
    public ChessBoard MoveWhileCastling(FigureMoving fm, String CastlingFen){
        logger.fine("Castling");
        ChessBoard next = new ChessBoard(CastlingFen);
        next.SetFigureAt(fm.from, Figure.none);
        next.SetFigureAt(fm.to, fm.promotion == Figure.none ? fm.figure : fm.promotion);

        next.moveColor = moveColor.FlipColor();
        next.GenerateFEN(); //we change FEN to remember steps
        return next;
    }

    public ChessBoard DeletingBadPawnAfterEnPassant(FigureMoving fm, String Fen){
        logger.fine("Pawn after En Passant deletes");
        ChessBoard next = new ChessBoard(Fen);
        next.SetFigureAt(new Piece(fm.to.x, GetColor(fm.figure) == Color.white ? fm.to.y - 1 : fm.to.y + 1), Figure.none);

        next.GenerateFEN(); //we change FEN to remember steps
        return next;
    }

    // Перебор всех фигур
    /**
     * Enumeration of all figures
     * @return Arraylist of all figures(consructor FigureOnPiece: figure and piece)
     * */
    public Iterable<FigureOnPiece> YieldFigures() {
        ArrayList<FigureOnPiece> result = new ArrayList<FigureOnPiece>();
        for (Piece piece: Piece.YieldPieces()) {
            if(GetColor(GetFigureAt(piece)) == moveColor){
                result.add(new FigureOnPiece(GetFigureAt(piece),piece));
            }
        }
        return result;
    }

    /**
     * It checks if the king is under check
     * @return true if some figure can move to piece of bad King,
     *        false if no one can eat badKing
     * */
    private boolean CanEatKing() {
        Piece badKing = FindBadKing(); // We find the piece of bad King
        Moves moves = new Moves(this);
        for (FigureOnPiece fp : YieldFigures()) {
            FigureMoving fm = new FigureMoving(fp, badKing, Figure.none); // we should go on piece of bad King
            if(moves.CanMove(fm)){
                return true;
            }
        }
        return false;
    }

    /**
     * This method finds bad King
     * @return piece(coordinates where bad King is now) or none piece if hi didn't find
     * */
    public Piece FindBadKing() {
        Figure badKing = moveColor == Color.black ? Figure.whiteKing : Figure.blackKing;
        for (Piece piece : Piece.YieldPieces()) {
            if(GetFigureAt(piece) == badKing){
                return piece;
            }
        }
        return Piece.none;
    }

    /**
     *  Verifies if the king is under check now
     * @return true if is and false if is not
     * */
    public boolean IsCheck(){
        logger.fine("Checks if you are under check now");
        ChessBoard after = new ChessBoard(fen);
        after.moveColor = moveColor.FlipColor();
        return after.CanEatKing();

    }

    /**
     *  Verifies if the king is under check after move
     * @param fm King move
     * @return true if is and false if is not
     * */
    public boolean IsCheckAfterMove(FigureMoving fm){
        logger.fine("Checks if you are under check after");
        if(moveColor == GetColor(fm.figure)){
            ChessBoard after = Move(fm);
            return after.CanEatKing();
        }
        return false;
    }
}
