package Engine.Pieces;

/**
 * Enumeration of Figures with identification text
 * */
public enum Figure {
    none('.'),

    whiteKing('K'),
    whiteQueen('Q'),
    whiteRook('R'),
    whiteBishop('B'),
    whiteKnight('N'),
    whitePawn('P'),

    blackKing('k'),
    blackQueen('q'),
    blackRook('r'),
    blackBishop('b'),
    blackKnight('n'),
    blackPawn('p')
    ;

    private char text;

    /**
     * Constructor for text
     * @param text char
     * */
    Figure(char text){
        this.text = text;
    }
    /**
     * Function for getting text of certain figure
     * @return char, text
     * */
    public char GetText(){
        return text;
    }

    /**
     * Function for getting Figure of certain text
     * @param text char
     * @return Figure, figure
     * */
    public static Figure GetFigure(char text){
        if (text == 'K'){
            return Figure.whiteKing;
        } else if(text == 'Q'){
            return Figure.whiteQueen;
        } else if(text == 'R'){
            return Figure.whiteRook;
        } else if(text == 'B'){
            return Figure.whiteBishop;
        } else if(text == 'N'){
            return Figure.whiteKnight;
        } else if(text == 'P'){
            return Figure.whitePawn;
        } else if(text == 'k'){
            return Figure.blackKing;
        } else if(text == 'q'){
            return Figure.blackQueen;
        } else if(text == 'r'){
            return Figure.blackRook;
        } else if(text == 'b'){
            return Figure.blackBishop;
        } else if(text == 'n'){
            return Figure.blackKnight;
        } else if(text == 'p'){
            return Figure.blackPawn;
        } else {
            return Figure.none;
        }
    }

    /**
     * Function for getting Color of Figure
     * @param figure
     * @return Color, color of given figure
     * */
    public static Color GetColor(Figure figure){
        if(figure == Figure.none){
            return Color.none;
        }
        return (figure == Figure.whiteKing ||
                figure == Figure.whiteQueen ||
                figure == Figure.whiteRook ||
                figure == Figure.whiteBishop ||
                figure == Figure.whiteKnight ||
                figure == Figure.whitePawn)
                ? Color.white : Color.black;
    }
}

