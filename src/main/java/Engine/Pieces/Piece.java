package Engine.Pieces;

import java.util.ArrayList;

public class Piece {
    public static Piece none = new Piece(-1,-1);
    public int x;
    public int y;

    public Piece(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Translation from String to used values
     * */
    public Piece(String e2) {
        if(e2.length() == 2 &&
           e2.charAt(0) >= 'a' &&
           e2.charAt(0) <= 'h' &&
           e2.charAt(1) >= '1' &&
           e2.charAt(1) <= '8'){
            x = e2.charAt(0) - 'a';
            y = e2.charAt(1) - '1';
        } else{
            this.x = -1;
            this.y = -1;
        }
    }

    /**
     * Enumeration of Pieces for enumeration of Figures
     * (YieldFigures()) in class chessboard
     * @return ArrayList<Piece> with all pieces
     * */
    public static Iterable<Piece> YieldPieces() {
        ArrayList<Piece> result = new ArrayList<Piece>();
        for(int y = 0; y < 8; y++){
            for(int x = 0; x < 8; x++){
                result.add(new Piece(x,y));
            }
        }
        return result;
    }

    /**
     * Function for checking OutOfBounds
     * */
    public boolean OnBoard(){
        return x >= 0 && x < 8 &&
                y >= 0 && y < 8;
    }

    /**
     * @return String, piece in string ("e2")
     * */
    public String Name(){
        return (char)('a' + x) + String.valueOf(y + 1);
    }
}
