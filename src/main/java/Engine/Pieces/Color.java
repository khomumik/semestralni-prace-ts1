package Engine.Pieces;

public enum Color {
    none,
    white,
    black,
    ;

    /**
     * Function to change the color during the game
     * @return Color, if current color is black, FlipColor changes it white and vice versa
     * */
    public Color FlipColor() {
        if (this == Color.black) return Color.white;
        if (this == Color.white) return Color.black;
        return Color.none;
    }
}

