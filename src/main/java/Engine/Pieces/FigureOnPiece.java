package Engine.Pieces;

public class FigureOnPiece {
    public Figure figure;
    public Piece piece;

    /**
     * Constructor for class FigureOnPiece with variables figure and piece
     * class describes piece and what is on
     * */
    public FigureOnPiece (Figure figure, Piece piece){
        this.figure = figure;
        this.piece = piece;
    }
}
