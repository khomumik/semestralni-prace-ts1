package Engine.Pieces;

public class FigureMoving {
    public Figure figure;
    public Piece from;
    public Piece to;
    public Figure promotion;

    /**
     * Constructor for usual requests
     * */
    public FigureMoving(FigureOnPiece fp, Piece to, Figure promotion){
        this.figure = fp.figure;
        this.from = fp.piece;
        this.to = to;
        this.promotion = promotion;
    }

    /**
     * Constructor for direct requests in need of transformation String to constructor(figure, pieces)
     * @param move String representation of move
     * */
    public FigureMoving(String move){ // Pe2e4 Pe7e8Q
                                      // 01234 012345
        this.figure = Figure.GetFigure(move.charAt(0));
        this.from = new Piece(move.substring(1,3));
        this.to = new Piece(move.substring(3,5));
        if(Figure.GetColor(figure) == Color.white){
            if(to.y == 7){
                this.promotion = (move.length() == 6) || this.figure == Figure.whitePawn ?
                        Figure.whiteQueen/*Figure.GetFigure(move.charAt(5))*/ : Figure.none; // Usually Figure.none instead of whiteQueen
            } else {
                this.promotion = Figure.none;
            }
        }else {
            if (to.y == 0) {
                this.promotion = (move.length() == 6) || this.figure == Figure.blackPawn?
                        Figure.blackQueen/*Figure.GetFigure(move.charAt(5))*/ : Figure.none; // Usually Figure.none instead of blackQueen
            } else {
                this.promotion = Figure.none;
            }
        }
    }

    /**
     * Functions for class Moves, is used to know direction
     * */
    public int GetDeltaX(){ return to.x - from.x; }
    public int GetDeltaY(){ return to.y - from.y; }

    /**
     * Functions for class Moves, is used to know distance
     * */
    public int GetAbsDeltaX(){ return Math.abs(GetDeltaX()); }
    public int GetAbsDeltaY(){ return Math.abs(GetDeltaY()); }

    /**
     * Function using for setting the direction in some functions in move
     * */
    public int GetSignX(){ return Integer.signum(GetDeltaX()); }
    public int GetSignY(){ return Integer.signum(GetDeltaY()); }

    @Override
    public String toString(){
        String text =  figure.GetText() + from.Name() + to.Name();
        if(promotion == Figure.none){
            text += promotion.GetText();
        }
        return text;
    }
}
