package Engine.Moves_and_Game;

import Engine.ChessBoard.ChessBoard;
import Engine.Pieces.*;

import java.util.ArrayList;
import java.util.logging.Logger;

import static Engine.Pieces.Figure.GetColor;

public class Chess {
    public String fen;
    public ChessBoard board;
    Moves moves;
    public ArrayList<FigureMoving> allMoves;

    private static final Logger logger = Logger.getLogger(Chess.class.getName());

    public Chess(String fen){
        this.fen = fen;
        board = new ChessBoard(fen);
        moves = new Moves(board);
    }

    /**
     * Constructor that makes new board after move
     * @param board relevant chessboard
     * */
    Chess(ChessBoard board) {
        this.board = board;
        fen = board.fen;
        moves = new Moves(board);
    }

    /**
     * Makes move, also castling is described here
     * @param move string with instructions for move "Pe2e4"
     *             or with promotion "Pe7e8Q"
     * @return constructor Chess with new board
     * */
    public Chess Move(String move){                 // Когда делаем новый ход
        logger.info("New move");
        FigureMoving fm = new FigureMoving(move);   // Мы генерируем ход
        if(!moves.CanMove(fm)) {
            logger.info("Sorry, but you can't move like this!");
            return this;
        }
        if(board.IsCheckAfterMove(fm)) {
            logger.info("Sorry, but you are under the check after move!");
            return this;
        }

        ChessBoard nextBoard = board.Move(fm);      // Создаём доску после выполнения хода

        // If En Passant we need to delete Pawn from chessboard
        if(fm.figure == Figure.whitePawn || fm.figure == Figure.blackPawn){
            int stepY = GetColor(fm.figure) == Color.white ? 1 : -1;
            if(Moves.CanPawnEatEnPassant(board, fm, stepY)){
                nextBoard = board.DeletingBadPawnAfterEnPassant(fm, nextBoard.fen);

            }
        }
        //  If Castling we need to move Rook also
        if(fm.figure == Figure.whiteKing || fm.figure == Figure.blackKing) {
            if(fm.figure == Figure.blackKing){ // King can do Castling just one time
                Game.BlackKingMoves += 1;
            } else {
                Game.WhiteKingMoves += 1;
            }
            if(fm.GetAbsDeltaX() == 2){
                String RookMove = "";
                Piece RookPiece;
                if(board.moveColor == Color.white){ RookMove += "R"; }
                    else { RookMove += "r"; }
                if(fm.GetSignX() == 1){ // King Side Castling
                    RookPiece = new Piece(fm.to.x - 1, fm.to.y);
                    RookMove +=  WhatChar(fm.to.x + 1) + (fm.to.y+1) + WhatChar(RookPiece.x) + (RookPiece.y+1);
                } else { // Queen Side Castling
                    RookPiece = new Piece(fm.to.x + 1, fm.to.y);
                    RookMove +=  WhatChar(fm.to.x - 2) + (fm.to.y+1) + WhatChar(RookPiece.x) + (RookPiece.y+1);
                }
                FigureMoving Rook = new FigureMoving(RookMove);
                if(!moves.CanMove(Rook)) {
                    logger.info("Sorry, but Rook can't move like this!");
                    return this;
                }
                if(board.IsCheckAfterMove(Rook)) {
                    logger.info("Sorry, but you are under the check after move!");
                    return this;
                }
                nextBoard = board.MoveWhileCastling(Rook, nextBoard.fen);
            }
        }
        return new Chess(nextBoard);
    }

    /**
     * Converts numbers to letters for using it in String move
     * @param ThisChar some number what equals some letter on chessboard
     * @return suitable letter
     * */
    public String WhatChar(Integer ThisChar){
        if(ThisChar == 0){
            return "a";
        } else if(ThisChar == 1){
            return "b";
        } else if(ThisChar == 2){
            return "c";
        } else if(ThisChar == 3){
            return "d";
        } else if(ThisChar == 4){
            return "e";
        } else if(ThisChar == 5){
            return "f";
        } else if(ThisChar == 6){
            return "g";
        } else {
            return "h";
        }
    }

    /**
     * @param x coordinates on axis x
     * @param y coordinates on axis y
     * @return char representation of figure on Piece(x, y)
     * */
    public char GetFigureAt(int x, int y){
        Piece piece = new Piece(x, y);
        Figure f = board.GetFigureAt(piece);
        if (f == null){
            board.SetFigureAt(piece, Figure.none);
            f = board.GetFigureAt(piece);
        }
        return f.GetText();
    }

    /**
     * Finds all possible moves and change the variable allMoves
     * */
    public void FindAllMoves(){
        allMoves = new ArrayList<FigureMoving>();
        for (FigureOnPiece fp: board.YieldFigures()) {
            for(Piece to: Piece.YieldPieces()){
                    FigureMoving fm = new FigureMoving(fp, to, Figure.none);
                    if(moves.CanMove(fm)){
                        if(!board.IsCheckAfterMove(fm)) {
                            allMoves.add(fm);
                        }
                    }
            }
        }
    }

    /**
     * Gives all possible moves as Arraylist os strings
     * @return Arraylist of strings as moves
     * */
    public ArrayList<String> GetAllMoves(){
        FindAllMoves();
        ArrayList<String> list = new ArrayList<String>();
        for (FigureMoving fm : allMoves) {
            list.add(fm.toString().substring(0,5));  // не забудь что ты здесь убираешь последний символ
        }
        return list;
    }

    /**
     *  Verifies if the king is under check now
     * @return true if is and false if is not
     * */
    public boolean IsCheck(){
        return board.IsCheck();
    }
}
