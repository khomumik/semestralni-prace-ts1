package Engine.Moves_and_Game;

import Engine.ChessBoard.ChessBoard;
import Engine.Pieces.*;

import static Engine.Pieces.Figure.GetColor;

public class Moves {
     FigureMoving fm;
     ChessBoard board;

     /**
      * Constructor to change lokal board
      * */
     public Moves(ChessBoard board){
          this.board = board;
     }

     /**
      * Main function calling others, decides can figure move to place player wants or cann't
      * @param fm (constructor from class FigureMoving) moving figure
      *           with coordinates from, where, what kind of figure and promotion
      * @return boolean true if can figure move and false if cann't
      * */
     public boolean CanMove(FigureMoving fm){
          this.fm = fm;
          return CanMoveFrom() &&
                  CanMoveTo() &&
                  CanFigureMove();
     }

     /**
      * Verifies if figure can move from given piece,
      * if piece on board and he is moving(it's about color, who is moving)
      * @return boolean true if can figure move from and false if cann't
      * */
     boolean CanMoveFrom(){
          return fm.from.OnBoard() &&
                  GetColor(fm.figure) == board.moveColor;
     }

     // also it makes rule that you can't eat yout own figures
     /**
      * Verifies if figure can move to given piece,
      * if piece on board and he is moving(it's about color, who is moving) and, of course,
      * if this piece is not the same piece it goes from
      * @return boolean true if can figure move to and false if cann't
      * */
     boolean CanMoveTo(){
          return fm.to.OnBoard() &&
                  fm.from != fm.to &&  // you can't go on the same piece you have been before
                  GetColor(board.GetFigureAt(fm.to)) != board.moveColor;  //If it's white's move, black can't move
     }

     /**
      * Verifies the right of move of specific figure(for convenience, divided into two)
      * @return boolean true if can figure move to and false if cann't
      * */
     boolean CanFigureMove(){
          switch (fm.figure){
               case whiteKing:
               case blackKing:
                    return CanKingMove();

               case whiteQueen:
               case blackQueen:
                    return CanStraightMove();

               case whiteRook:
               case blackRook:
                    return (fm.GetSignX() == 0 || fm.GetSignY() == 0) && CanStraightMove();

               case whiteBishop:
               case blackBishop:
                    return (fm.GetSignX() != 0 && fm.GetSignY() != 0) && CanStraightMove();

               case whiteKnight:
               case blackKnight:
                    return CanKnightMove();

               case whitePawn:
               case blackPawn:
                    return CanPawnMove();
                    
               default: return false;
          }
     }

     /**
      * Verifies the right of Pawn to move
      * @return boolean true if can pawn move to and false if cann't
      * */
     private boolean CanPawnMove() {
          if(fm.from.y < 1 || fm.from.y > 6){  // Pawn cann't be on first and last horizontal
               return false;
          }
          int stepY = GetColor(fm.figure) == Color.white ? 1 : -1;  // For how much Pawn should go
          return CanPawnGo(stepY) ||
                  CanPawnJump(stepY) ||
                  CanPawnEat(stepY) ||
                  CanPawnEatEnPassant(board, fm, stepY);
     }

     public static boolean CanPawnEatEnPassant(ChessBoard board, FigureMoving fm, int stepY) {
          if(GetColor(fm.figure) == Color.white) {
               if (board.GetFigureAt(fm.to) == Figure.none) {
                    if (board.GetFigureAt(new Piece(fm.from.x + 1, fm.from.y)) == Figure.blackPawn ||
                            board.GetFigureAt(new Piece(fm.from.x - 1, fm.from.y)) == Figure.blackPawn) {
                         if(fm.from.y == 4){
                              return fm.GetDeltaY() == stepY || fm.GetAbsDeltaX() == 1;
                         }
                    }
               }
          }else {
               if (board.GetFigureAt(fm.to) == Figure.none) {
                    if (board.GetFigureAt(new Piece(fm.from.x + 1, fm.from.y)) == Figure.whitePawn ||
                            board.GetFigureAt(new Piece(fm.from.x - 1, fm.from.y)) == Figure.whitePawn) {
                         if(fm.from.y == 3){
                              return fm.GetDeltaY() == stepY || fm.GetAbsDeltaX() == 1;
                         }
                    }
               }
          }
          return false;
     }

     /**
      * Verifies the right of pawn to eat
      * @param stepY int describes move of Pawn
      * @return boolean true if can pawn eat someone and false if cann't
      * */
     private boolean CanPawnEat(int stepY) {
          if(board.GetFigureAt(fm.to) != Figure.none){
               if(fm.GetAbsDeltaX() == 1){
                    return fm.GetDeltaY() == stepY;
               }
          }
          return false;
     }

     /**
      * Verifies the right of pawn to jump
      * @return boolean true if can pawn jump(first step when pawn can jump over 1 tile) and false if cann't
      * */
     private boolean CanPawnJump(int stepY) {
          if (board.GetFigureAt(fm.to) == Figure.none){
               if(fm.GetDeltaX() == 0){
                    if(fm.GetDeltaY() == 2 * stepY){
                         if(fm.from.y == 1 || fm.from.y == 6){
                              return board.GetFigureAt(new Piece(fm.from.x, fm.from.y + stepY)) == Figure.none;
                         }
                    }
               }
          }
          return false;
     }
     /**
      * Verifies the right of pawn to go
      * @return boolean true if can pawn go(in usual way now by 1 step) and false if cann't
      * */
     private boolean CanPawnGo(int stepY) {
          if(board.GetFigureAt(fm.to) == Figure.none){
               return fm.GetDeltaX() == 0 && fm.GetDeltaY() == stepY;
          }
          return false;
     }

     /**
      * Verifies if figures with straight moves can go wherever they want
      * @return boolean true if can figure go on given piece and false if cann't
      * */
     private boolean CanStraightMove() {
          Piece at = fm.from;
          do {
               at = new Piece(at.x + fm.GetSignX(), at.y + fm.GetSignY());   // We create new Piece where will be constant bias(смещение)
               if(at.x == fm.to.x && at.y == fm.to.y){
                    return true;
               }
          } while(at.OnBoard() && board.GetFigureAt(at) == Figure.none);
          return false;
     }

     /**
      * Verifies th right of Knight to go
      * @return boolean true if can knight go on given piece and false if cann't
      * */
     private boolean CanKnightMove() {
          if(fm.GetAbsDeltaX() == 1 && fm.GetAbsDeltaY() == 2) return true;
          return fm.GetAbsDeltaX() == 2 && fm.GetAbsDeltaY() == 1;
     }

     /**
      * Verifies the right of king to go(based on principles of CanStraightMove() and has more conditions)
      * @return boolean true if can King go on given piece and false if cann't
      * */
     private boolean CanKingMove() {
          Piece at = fm.from;
          do{
               at = new Piece(at.x + fm.GetSignX(), at.y);
               if(at.x == fm.to.x && at.y == fm.to.y){
                    return true;
               }
          } while(at.OnBoard() && board.GetFigureAt(at) == Figure.none &&
                  (((board.moveColor == Color.white ? board.GetFigureAt(new Piece(at.x + 2*fm.GetSignX(), at.y)) ==
                  Figure.whiteRook : board.GetFigureAt(new Piece(at.x + 2*fm.GetSignX(), at.y)) == Figure.blackRook) ||
                  (board.moveColor == Color.white ? board.GetFigureAt(new Piece(at.x + 3*fm.GetSignX(), at.y)) ==
                  Figure.whiteRook : board.GetFigureAt(new Piece(at.x + 3*fm.GetSignX(), at.y)) == Figure.blackRook))) &&
                  (fm.from.y == 0 || fm.from.y == 7) && (GetColor(fm.figure) == Color.white ? Game.WhiteKingMoves == 0 : Game.BlackKingMoves == 0));
          return fm.GetAbsDeltaX() <= 1 && fm.GetAbsDeltaY() <= 1;
     }
}
