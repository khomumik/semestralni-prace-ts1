package Engine.Moves_and_Game;

import GUI.Table;

import java.io.IOException;
import java.util.logging.Logger;

public class Game {
    public static int BlackKingMoves = 0;
    public static int WhiteKingMoves = 0;
    public static Chess chess = new Chess("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");

    private static final Logger logger = Logger.getLogger(Game.class.getName());

    /**
     * Main program,
     * what is commented was used for console output(ASCII ART)
     * now only lounches GUI
     * @param args console input
     * */
    public static void main(String[] args) throws IOException, InterruptedException {
//        Random random = new Random();
//        ArrayList<String> list;
//        Scanner sc = new Scanner(System.in);
        logger.info("GUI is ON");
        Table table = new Table(chess.board);
//        while (true){
//           list = chess.GetAllMoves();
//            System.out.println(chess.fen);
//            System.out.println(ChessToAscii(chess));

//            System.out.println(chess.IsCheck() ? "CHECK" : "-");
//            if(chess.IsCheck() && list.size() == 0){
//                System.out.print("mate!");
//                break;
//            } else if(list.size()== 0){
//                System.out.print("Stalemate!");
//                break;
//            }
//            for (String moves : list) {
//                System.out.print(moves + "\t");
//            }
//            System.out.println();
//            System.out.print("> ");
//            String move = sc.nextLine();
//            if (move.equals("q")) break;
//            if(move.equals("")){
//                move = list.get(random.nextInt(list.size()));
//            }
//            chess = chess.Move(move);
//        }
    }

    /**
     * @deprecated was used as output of chessbourd in console as ASCII ART
     * @return String text
     * */
    static String ChessToAscii(Chess chess){
        String text = "  +-----------------+\n";
        for(int y = 7; y >= 0; y--){
            text += y + 1;
            text += " | ";
            for(int x = 0; x < 8; x++){
                text += chess.GetFigureAt(x,y) + " ";
            }
            text += "|\n";
        }
        text +="  +-----------------+\n";
        text +="    a b c d e f g h\n";
        return text;
    }

}
